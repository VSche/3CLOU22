resource "scaleway_instance_ip" "public_ip1" {
  project_id = var.project_id
}

resource "scaleway_instance_ip" "public_ip2" {
  project_id = var.project_id
}
 

resource "scaleway_instance_server" "www1" {
  name = "${local.team}-www1"

  project_id = var.project_id
  type       = "DEV1-L"
  image      = "ubuntu_jammy"

  tags = ["front", "web"]

  ip_id = scaleway_instance_ip.public_ip1.id

  #additional_volume_ids = [scaleway_instance_volume.data.id]

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 50
  }

  security_group_id = scaleway_instance_security_group.sg-www.id
}


resource "scaleway_instance_server" "www2" {
  name = "${local.team}-www2"

  project_id = var.project_id
  type       = "DEV1-L"
  image      = "ubuntu_jammy"

  tags = ["front", "web"]

  ip_id = scaleway_instance_ip.public_ip2.id

  #additional_volume_ids = [scaleway_instance_volume.data.id]

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 50
  }

  security_group_id = scaleway_instance_security_group.sg-www.id
}

output "www1" {
  value = "${scaleway_instance_server.www1.public_ip}"
}

output "www2" {
  value = "${scaleway_instance_server.www2.public_ip}"
}
